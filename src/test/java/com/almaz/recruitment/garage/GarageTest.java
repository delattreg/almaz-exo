package com.almaz.recruitment.garage;

import org.junit.Test;

import static com.almaz.recruitment.garage.Car.GERMAN_CAR;
import static com.almaz.recruitment.garage.Car.REGULAR_CAR;
import static org.junit.Assert.assertEquals;

/*
 * Copyright (c) 2016 Almaz Informatique SA
 *
 */
public class GarageTest {

    @Test
    public void testSimpleGarage() {

        Garage g = new SimpleGarage();

        assertEquals(0, g.schedule(REGULAR_CAR, Job.CHECKUP));
        assertEquals(0, g.schedule(REGULAR_CAR, Job.TROUBLESHOOT));
        assertEquals(0, g.schedule(REGULAR_CAR, Job.SERVICE));
    }

    @Test
    public void testPaintJob() {

        Garage g = new SimpleGarage();

        assertEquals(2, g.schedule(REGULAR_CAR, Job.PAINT));
    }

    @Test
    public void testRegularGarage() {

        Garage g = new RegularGarage(8);

        assertEquals(0, g.schedule(REGULAR_CAR, Job.SERVICE));
        assertEquals(0, g.schedule(REGULAR_CAR, Job.TROUBLESHOOT));
        assertEquals(0, g.schedule(REGULAR_CAR, Job.SERVICE));

//        assertEquals(1, g.schedule(REGULAR_CAR, Job.TROUBLESHOOT));
        assertEquals(3, g.schedule(REGULAR_CAR, Job.PAINT));
        assertEquals(2, g.schedule(REGULAR_CAR, Job.CHECKUP));
    }

    @Test
    public void testGermanCar() {

        Garage g = new RegularGarage(8);

        assertEquals(0, g.schedule(GERMAN_CAR, Job.SERVICE));
        assertEquals(1, g.schedule(GERMAN_CAR, Job.CHECKUP));
//        assertEquals(1, g.schedule(REGULAR_CAR, Job.SERVICE));
    }

    /**
     * Bob has documented how a {@link LargeGarage} works, but has not had time to write a proper test case. Please
     * be a boy scout and write one yourself.
     */
    @Test
    public void testLargeGarage() {
        final Garage g = new LargeGarage(8);

        assertEquals(0, g.schedule(REGULAR_CAR, Job.CHECKUP));
        assertEquals(0, g.schedule(REGULAR_CAR, Job.TROUBLESHOOT));
        assertEquals(0, g.schedule(REGULAR_CAR, Job.SERVICE));
    }


    @Test(expected = IllegalArgumentException.class)
    public void testWithNullValues() {
        final Garage g = new SimpleGarage();

        g.schedule(null, null);
    }
}
