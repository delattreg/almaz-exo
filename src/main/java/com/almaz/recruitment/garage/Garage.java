package com.almaz.recruitment.garage;

/*
 * Copyright (c) 2016 Almaz Informatique SA
 *
 * This interface represents a car service (garage).
 */
public interface Garage {
    /**
     * @param car The car to be scheduled
     * @param job The type of job that needs to be performed on the car
     * @return The number of days until the car is ready. 0 means today, 1 tomorrow, etc
     */
    default int schedule(Car car, Job job) {
        throw new UnsupportedOperationException();
    }
}
