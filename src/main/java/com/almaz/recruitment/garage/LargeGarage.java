package com.almaz.recruitment.garage;

/*
 * Copyright (c) 2016 Almaz Informatique SA
 *
 * The large garage has a limited number of working hours per day. Compared to other garages,
 * it has several pipelines, allowing it to work on several cars at the same time.
 *
 * Once a job is started, the job cannot switch pipelines. The less busy pipeline will be chosen.
 */
public class LargeGarage implements Garage {

    private int hours;

    public LargeGarage(int hours) {
        this.hours = hours;
    }

    @Override
    public int schedule(final Car car, final Job job) {
        if (null== car || null == job) {
            throw new IllegalArgumentException("com.almaz.recruitment.garage.LargeGarage#schedule() method " +
                    "should be used with non-null arguments");
        }
        switch (car) {
            case GERMAN_CAR: return scheduleForGermanCar(job);
            case REGULAR_CAR: return scheduleForRegularCar(job);
            default : Garage.super.schedule(car, job);
                break;
        }
        return 0;
    }

    private int scheduleForRegularCar(final Job job) {
        switch (job) {
            case SERVICE : return 2 / hours;
            case CHECKUP : return 3 / hours;
            case TROUBLESHOOT : return 4 / hours;
            case PAINT: return 3;
        }
        return 0;
    }

    private int scheduleForGermanCar(final Job job) {
        switch (job) {
            case SERVICE : return 4 / hours;
            case CHECKUP : return 6 / hours;
            case TROUBLESHOOT : return 8 / hours;
            case PAINT: return 3;
        }
        return 0;
    }

}
