package com.almaz.recruitment.garage;

/*
 * Copyright (c) 2016 Almaz Informatique SA
 *
 * This simple garage has no capacity or working hour constraints, it can work on any number of cars at the same time.
 */
public class SimpleGarage implements Garage {

    @Override
    public int schedule(final Car car, final Job job) {
        if (null== car || null == job) {
            throw new IllegalArgumentException("com.almaz.recruitment.garage.SimpleGarage#schedule() method " +
                    "should be used with non-null arguments");
        }
        switch (car) {
            case GERMAN_CAR: return scheduleForGermanCar(job);
            case REGULAR_CAR: return scheduleForRegularCar(job);
            default : Garage.super.schedule(car, job);
                      break;
        }
        return 0;
    }

    private int scheduleForRegularCar(final Job job) {
        switch (job) {
            case SERVICE :
            case CHECKUP :
            case TROUBLESHOOT :
                return 0;
            case PAINT:
                return 2;
        }
        return 0;
    }

    private int scheduleForGermanCar(final Job job) {
        switch (job) {
            case SERVICE :
            case CHECKUP :
            case TROUBLESHOOT :
                return 0;
            case PAINT:
                return 3;
        }
        return 0;
    }
}