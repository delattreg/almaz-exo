package com.almaz.recruitment.garage;

/*
 * Copyright (c) 2016 Almaz Informatique SA
 *
 * The regular garage can work on one car at a time, and has a limited number of working hours per day.
 */
public class RegularGarage implements Garage {

    private int hours;

    public RegularGarage(int hours) {
        this.hours = hours;
    }

    @Override
    public int schedule(final Car car, final Job job) {
        if (null== car || null == job) {
            throw new IllegalArgumentException("com.almaz.recruitment.garage.RegularGarage#schedule() method " +
                    "should be used with non-null arguments");
        }
        switch (car) {
            case GERMAN_CAR: return scheduleForGermanCar(job);
            case REGULAR_CAR: return scheduleForRegularCar(job);
            default : Garage.super.schedule(car, job);
                break;
        }
        return 0;
    }

    private int scheduleForRegularCar(final Job job) {
        switch (job) {
            case SERVICE : return 2 / hours;
            case CHECKUP : return 4 / hours + 2;
            case TROUBLESHOOT : return 4 / hours;
            case PAINT: return 3;
        }
        return 0;
    }

    private int scheduleForGermanCar(final Job job) {
        switch (job) {
            case SERVICE : return 4 / hours;
            case CHECKUP : return 6 / hours + 1;
            case TROUBLESHOOT : return 8 / hours;
            case PAINT: return 3;
        }
        return 0;
    }
}
